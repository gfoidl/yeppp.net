﻿namespace System
{
	public static class MathExtensions_EvaluatePolynomial
	{
		/// <summary>Evaluates polynomial with single precision (32-bit) floating-point coefficients on an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If coefArray, xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If coefArray, xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If coefCount or length is negative or coefCount is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If coefOffset is negative, coefOffset + coefCount exceeds the length of coefArray, xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, coefCount is negative, or length is negative.</exception>
		public static float[] EvaluatePolynomial(this float[] xArray, float[] coeffArray)
		{
			float[] result = new float[xArray.Length];

			Yeppp.Math.EvaluatePolynomial_V32fV32f_V32f(coeffArray, 0, xArray, 0, result, 0, coeffArray.Length, result.Length);

			return result;
		}
		//---------------------------------------------------------------------
		/// <summary>Evaluates polynomial with double precision (64-bit) floating-point coefficients on an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If coefArray, xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If coefArray, xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If coefCount or length is negative or coefCount is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If coefOffset is negative, coefOffset + coefCount exceeds the length of coefArray, xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, coefCount is negative, or length is negative.</exception>
		public static double[] EvaluatePolynomial(this double[] xArray, double[] coeffArray)
		{
			double[] result = new double[xArray.Length];

			Yeppp.Math.EvaluatePolynomial_V64fV64f_V64f(coeffArray, 0, xArray, 0, result, 0, coeffArray.Length, result.Length);

			return result;
		}
	}
}