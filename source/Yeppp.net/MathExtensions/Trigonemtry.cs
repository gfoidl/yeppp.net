﻿namespace System
{
	public static class MathExtensions_Trigonemtry
	{
		/// <summary>Computes cosine on double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Cos(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Math.Cos_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes sine on double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Sin(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Math.Sin_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes tangent on double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Tan(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Math.Tan_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
	}
}