﻿namespace System
{
	public static class MathExtensions_ExpLog
	{
		/// <summary>Computes exponent on double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Exp(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Math.Exp_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes natural logarithm on double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Log(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Math.Log_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
	}
}