﻿namespace System
{
	public static class CoreExtensions_DotProduct
	{
		#region float
		/// <summary>Computes the dot product of two vectors of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static float DotProduct(this float[] xArray, float[] yArray)
		{
			return Yeppp.Core.DotProduct_V32fV32f_S32f(xArray, 0, yArray, 0, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Computes the dot product of two vectors of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double DotProduct(this double[] xArray, double[] yArray)
		{
			return Yeppp.Core.DotProduct_V64fV64f_S64f(xArray, 0, yArray, 0, xArray.Length);
		}
		#endregion
	}
}