﻿namespace System
{
	public static class CoreExtensions_SumAbs
	{
		#region float
		/// <summary>Computes the sum of absolute values of single precision (32-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static float SumAbs(this float[] xArray)
		{
			return Yeppp.Core.SumAbs_V32f_S32f(xArray, 0, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Computes the sum of absolute values of double precision (64-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static double SumAbs(this double[] xArray)
		{
			return Yeppp.Core.SumAbs_V64f_S64f(xArray, 0, xArray.Length);
		}
		#endregion
	}
}