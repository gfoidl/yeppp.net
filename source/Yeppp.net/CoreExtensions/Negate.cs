﻿namespace System
{
	public static class CoreExtensions_Negate
	{
		#region int
		/// <summary>Negates elements in signed 32-bit integer array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static int[] Negate(this int[] xArray)
		{
			int[] yArray = new int[xArray.Length];

			Yeppp.Core.Negate_V32s_V32s(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Negates elements in signed 32-bit integer array and writes the results to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static void NegateAndReplace(this int[] xArray)
		{
			Yeppp.Core.Negate_IV32s_IV32s(xArray, 0, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Negates elements in signed 64-bit integer array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static long[] Negate(this long[] xArray)
		{
			long[] yArray = new long[xArray.Length];

			Yeppp.Core.Negate_V64s_V64s(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Negates elements in signed 64-bit integer array and writes the results to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static void NegateAndReplace(this long[] xArray)
		{
			Yeppp.Core.Negate_IV64s_IV64s(xArray, 0, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Negates elements in single precision (32-bit) floating-point array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static float[] Negate(this float[] xArray)
		{
			float[] yArray = new float[xArray.Length];

			Yeppp.Core.Negate_V32f_V32f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Negates elements in single precision (32-bit) floating-point array and writes the results to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static void NegateAndReplace(this float[] xArray)
		{
			Yeppp.Core.Negate_IV32f_IV32f(xArray, 0, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Negates elements in double precision (64-bit) floating-point array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static double[] Negate(this double[] xArray)
		{
			double[] yArray = new double[xArray.Length];

			Yeppp.Core.Negate_V64f_V64f(xArray, 0, yArray, 0, xArray.Length);

			return yArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Negates elements in double precision (64-bit) floating-point array and writes the results to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static void NegateAndReplace(this double[] xArray)
		{
			Yeppp.Core.Negate_IV64f_IV64f(xArray, 0, xArray.Length);
		}
		#endregion
	}
}