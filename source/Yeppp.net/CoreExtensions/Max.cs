﻿namespace System
{
	public static class CoreExtensions_Max
	{
		#region int
		/// <summary>Computes the maximum of signed 32-bit integer array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static int Max(this int[] xArray)
		{
			return Yeppp.Core.Max_V32s_S32s(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two signed 32-bit integer arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static int[] MaxPairwise(this int[] xArray, int[] yArray)
		{
			int[] maximumArray = new int[xArray.Length];

			Yeppp.Core.Max_V32sV32s_V32s(xArray, 0, yArray, 0, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of signed 32-bit integer array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static int[] MaxPairwise(this int[] xArray, int c)
		{
			int[] maximumArray = new int[xArray.Length];

			Yeppp.Core.Max_V32sS32s_V32s(xArray, 0, c, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two signed 32-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MaxAndReplace(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Max_IV32sV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of signed 32-bit integer array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MaxPairwiseAndReplace(this int[] xArray, int c)
		{
			Yeppp.Core.Max_IV32sS32s_IV32s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Computes the maximum of signed 64-bit integer array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static long Max(this long[] xArray)
		{
			return Yeppp.Core.Max_V64s_S64s(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two signed 64-bit integer arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static long[] MaxPairwise(this long[] xArray, int[] yArray)
		{
			long[] maximumArray = new long[xArray.Length];

			Yeppp.Core.Max_V64sV32s_V64s(xArray, 0, yArray, 0, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of signed 64-bit integer array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static long[] MaxPairwise(this long[] xArray, int c)
		{
			long[] maximumArray = new long[xArray.Length];

			Yeppp.Core.Max_V64sS32s_V64s(xArray, 0, c, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two signed 64-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MaxAndReplace(this long[] xArray, int[] yArray)
		{
			Yeppp.Core.Max_IV64sV32s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of signed 64-bit integer array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MaxPairwiseAndReplace(this long[] xArray, int c)
		{
			Yeppp.Core.Max_IV64sS32s_IV64s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Computes the maximum of single precision (32-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static float Max(this float[] xArray)
		{
			return Yeppp.Core.Max_V32f_S32f(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two single precision (32-bit) floating-point arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static float[] MaxPairwise(this float[] xArray, float[] yArray)
		{
			float[] maximumArray = new float[xArray.Length];

			Yeppp.Core.Max_V32fV32f_V32f(xArray, 0, yArray, 0, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of single precision (32-bit) floating-point array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static float[] MaxPairwise(this float[] xArray, float c)
		{
			float[] maximumArray = new float[xArray.Length];

			Yeppp.Core.Max_V32fS32f_V32f(xArray, 0, c, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MaxAndReplace(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Max_IV32fV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of single precision (32-bit) floating-point array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MaxPairwiseAndReplace(this float[] xArray, float c)
		{
			Yeppp.Core.Max_IV32fS32f_IV32f(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Computes the maximum of double precision (64-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static double Max(this double[] xArray)
		{
			return Yeppp.Core.Max_V64f_S64f(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two double precision (64-bit) floating-point arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static double[] MaxPairwise(this double[] xArray, double[] yArray)
		{
			double[] maximumArray = new double[xArray.Length];

			Yeppp.Core.Max_V64fV64f_V64f(xArray, 0, yArray, 0, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of double precision (64-bit) floating-point array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or maximumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or maximumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, maximumOffset is negative, maximumOffset + length exceeds the length of maximumArray, or length is negative.</exception>
		public static double[] MaxPairwise(this double[] xArray, double c)
		{
			double[] maximumArray = new double[xArray.Length];

			Yeppp.Core.Max_V64fS64f_V64f(xArray, 0, c, maximumArray, 0, maximumArray.Length);

			return maximumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MaxAndReplace(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Max_IV64fV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise maxima of single precision (32-bit) floating-point array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MaxPairwiseAndReplace(this double[] xArray, double c)
		{
			Yeppp.Core.Max_IV64fS64f_IV64f(xArray, 0, c, xArray.Length);
		}
		#endregion
	}
}