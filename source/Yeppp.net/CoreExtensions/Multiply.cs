﻿namespace System
{
	public static class CoreExtensions_Multiply
	{
		#region int
		/// <summary>Multiplys corresponding elements in two signed 32-bit integer arrays. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static int[] Multiply(this int[] xArray, int[] yArray)
		{
			int[] productArray = new int[xArray.Length];

			Yeppp.Core.Multiply_V32sV32s_V32s(xArray, 0, yArray, 0, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to signed 32-bit integer array elements. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static int[] Multiply(this int[] xArray, int c)
		{
			int[] productArray = new int[xArray.Length];

			Yeppp.Core.Multiply_V32sS32s_V32s(xArray, 0, c, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys corresponding elements in two signed 32-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Multiply_IV32sV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to signed 32-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this int[] xArray, int c)
		{
			Yeppp.Core.Multiply_IV32sS32s_IV32s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Multiplys corresponding elements in two signed 64-bit integer arrays. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static long[] Multiply(this long[] xArray, long[] yArray)
		{
			long[] productArray = new long[xArray.Length];

			Yeppp.Core.Multiply_V64sV64s_V64s(xArray, 0, yArray, 0, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to signed 64-bit integer array elements. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static long[] Multiply(this long[] xArray, long c)
		{
			long[] productArray = new long[xArray.Length];

			Yeppp.Core.Multiply_V64sS64s_V64s(xArray, 0, c, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys corresponding elements in two signed 64-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this long[] xArray, long[] yArray)
		{
			Yeppp.Core.Multiply_IV64sV64s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to signed 64-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this long[] xArray, long c)
		{
			Yeppp.Core.Multiply_IV64sS64s_IV64s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Multiplys corresponding elements in two single precision (32-bit) floating-point arrays. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static float[] Multiply(this float[] xArray, float[] yArray)
		{
			float[] productArray = new float[xArray.Length];

			Yeppp.Core.Multiply_V32fV32f_V32f(xArray, 0, yArray, 0, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to single precision (32-bit) floating-point array elements. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static float[] Multiply(this float[] xArray, float c)
		{
			float[] productArray = new float[xArray.Length];

			Yeppp.Core.Multiply_V32fS32f_V32f(xArray, 0, c, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Multiply_IV32fV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to single precision (32-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this float[] xArray, float c)
		{
			Yeppp.Core.Multiply_IV32fS32f_IV32f(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Multiplys corresponding elements in two double precision (64-bit) floating-point arrays. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static double[] Multiply(this double[] xArray, double[] yArray)
		{
			double[] productArray = new double[xArray.Length];

			Yeppp.Core.Multiply_V64fV64f_V64f(xArray, 0, yArray, 0, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to double precision (64-bit) floating-point array elements. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or productArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or productArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of productArray, or length is negative.</exception>
		public static double[] Multiply(this double[] xArray, double c)
		{
			double[] productArray = new double[xArray.Length];

			Yeppp.Core.Multiply_V64fS64f_V64f(xArray, 0, c, productArray, 0, productArray.Length);

			return productArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Multiply_IV64fV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Multiplys a constant to double precision (64-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MultiplyAndReplace(this double[] xArray, double c)
		{
			Yeppp.Core.Multiply_IV64fS64f_IV64f(xArray, 0, c, xArray.Length);
		}
		#endregion
	}
}