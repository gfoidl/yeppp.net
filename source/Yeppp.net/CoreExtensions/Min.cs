﻿namespace System
{
	public static class CoreExtensions_Min
	{
		#region int
		/// <summary>Computes the minimum of signed 32-bit integer array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static int Min(this int[] xArray)
		{
			return Yeppp.Core.Min_V32s_S32s(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two signed 32-bit integer arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static int[] MinPairwise(this int[] xArray, int[] yArray)
		{
			int[] minimumArray = new int[xArray.Length];

			Yeppp.Core.Min_V32sV32s_V32s(xArray, 0, yArray, 0, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of signed 32-bit integer array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static int[] MinPairwise(this int[] xArray, int c)
		{
			int[] minimumArray = new int[xArray.Length];

			Yeppp.Core.Min_V32sS32s_V32s(xArray, 0, c, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two signed 32-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MinAndReplace(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Min_IV32sV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of signed 32-bit integer array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MinPairwiseAndReplace(this int[] xArray, int c)
		{
			Yeppp.Core.Min_IV32sS32s_IV32s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Computes the minimum of signed 64-bit integer array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static long Min(this long[] xArray)
		{
			return Yeppp.Core.Min_V64s_S64s(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two signed 64-bit integer arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static long[] MinPairwise(this long[] xArray, int[] yArray)
		{
			long[] minimumArray = new long[xArray.Length];

			Yeppp.Core.Min_V64sV32s_V64s(xArray, 0, yArray, 0, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of signed 64-bit integer array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static long[] MinPairwise(this long[] xArray, int c)
		{
			long[] minimumArray = new long[xArray.Length];

			Yeppp.Core.Min_V64sS32s_V64s(xArray, 0, c, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two signed 64-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MinAndReplace(this long[] xArray, int[] yArray)
		{
			Yeppp.Core.Min_IV64sV32s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of signed 64-bit integer array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MinPairwiseAndReplace(this long[] xArray, int c)
		{
			Yeppp.Core.Min_IV64sS32s_IV64s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Computes the minimum of single precision (32-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static float Min(this float[] xArray)
		{
			return Yeppp.Core.Min_V32f_S32f(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two single precision (32-bit) floating-point arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static float[] MinPairwise(this float[] xArray, float[] yArray)
		{
			float[] minimumArray = new float[xArray.Length];

			Yeppp.Core.Min_V32fV32f_V32f(xArray, 0, yArray, 0, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of single precision (32-bit) floating-point array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static float[] MinPairwise(this float[] xArray, float c)
		{
			float[] minimumArray = new float[xArray.Length];

			Yeppp.Core.Min_V32fS32f_V32f(xArray, 0, c, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MinAndReplace(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Min_IV32fV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of single precision (32-bit) floating-point array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MinPairwiseAndReplace(this float[] xArray, float c)
		{
			Yeppp.Core.Min_IV32fS32f_IV32f(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Computes the minimum of double precision (64-bit) floating-point array elements.</summary>
		/// <exception cref="System.NullReferenceException">If vArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If vArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative or length is zero.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If vOffset is negative, vOffset + length exceeds the length of vArray, or length is negative.</exception>
		public static double Min(this double[] xArray)
		{
			return Yeppp.Core.Min_V64f_S64f(xArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two double precision (64-bit) floating-point arrays.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static double[] MinPairwise(this double[] xArray, double[] yArray)
		{
			double[] minimumArray = new double[xArray.Length];

			Yeppp.Core.Min_V64fV64f_V64f(xArray, 0, yArray, 0, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of double precision (64-bit) floating-point array elements and a constant.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or minimumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or minimumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, minimumOffset is negative, minimumOffset + length exceeds the length of minimumArray, or length is negative.</exception>
		public static double[] MinPairwise(this double[] xArray, double c)
		{
			double[] minimumArray = new double[xArray.Length];

			Yeppp.Core.Min_V64fS64f_V64f(xArray, 0, c, minimumArray, 0, minimumArray.Length);

			return minimumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void MinAndReplace(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Min_IV64fV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Computes pairwise minima of single precision (32-bit) floating-point array elements and a constant and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void MinPairwiseAndReplace(this double[] xArray, double c)
		{
			Yeppp.Core.Min_IV64fS64f_IV64f(xArray, 0, c, xArray.Length);
		}
		#endregion
	}
}