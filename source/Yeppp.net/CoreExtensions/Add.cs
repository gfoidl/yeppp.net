﻿namespace System
{
	public static class CoreExtensions_Add
	{
		#region int
		/// <summary>Adds corresponding elements in two signed 32-bit integer arrays. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static int[] Add(this int[] xArray, int[] yArray)
		{
			int[] sumArray = new int[xArray.Length];

			Yeppp.Core.Add_V32sV32s_V32s(xArray, 0, yArray, 0, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to signed 32-bit integer array elements. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static int[] Add(this int[] xArray, int c)
		{
			int[] sumArray = new int[xArray.Length];

			Yeppp.Core.Add_V32sS32s_V32s(xArray, 0, c, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds corresponding elements in two signed 32-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void AddAndReplace(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Add_IV32sV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to signed 32-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void AddAndReplace(this int[] xArray, int c)
		{
			Yeppp.Core.Add_IV32sS32s_IV32s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Adds corresponding elements in two signed 64-bit integer arrays. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static long[] Add(this long[] xArray, long[] yArray)
		{
			long[] sumArray = new long[xArray.Length];

			Yeppp.Core.Add_V64sV64s_V64s(xArray, 0, yArray, 0, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to signed 64-bit integer array elements. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static long[] Add(this long[] xArray, long c)
		{
			long[] sumArray = new long[xArray.Length];

			Yeppp.Core.Add_V64sS64s_V64s(xArray, 0, c, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds corresponding elements in two signed 64-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void AddAndReplace(this long[] xArray, long[] yArray)
		{
			Yeppp.Core.Add_IV64sV64s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to signed 64-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void AddAndReplace(this long[] xArray, long c)
		{
			Yeppp.Core.Add_IV64sS64s_IV64s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Adds corresponding elements in two single precision (32-bit) floating-point arrays. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static float[] Add(this float[] xArray, float[] yArray)
		{
			float[] sumArray = new float[xArray.Length];

			Yeppp.Core.Add_V32fV32f_V32f(xArray, 0, yArray, 0, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to single precision (32-bit) floating-point array elements. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static float[] Add(this float[] xArray, float c)
		{
			float[] sumArray = new float[xArray.Length];

			Yeppp.Core.Add_V32fS32f_V32f(xArray, 0, c, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void AddAndReplace(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Add_IV32fV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to single precision (32-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void AddAndReplace(this float[] xArray, float c)
		{
			Yeppp.Core.Add_IV32fS32f_IV32f(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Adds corresponding elements in two double precision (64-bit) floating-point arrays. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static double[] Add(this double[] xArray, double[] yArray)
		{
			double[] sumArray = new double[xArray.Length];

			Yeppp.Core.Add_V64fV64f_V64f(xArray, 0, yArray, 0, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to double precision (64-bit) floating-point array elements. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or sumArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or sumArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of sumArray, or length is negative.</exception>
		public static double[] Add(this double[] xArray, double c)
		{
			double[] sumArray = new double[xArray.Length];

			Yeppp.Core.Add_V64fS64f_V64f(xArray, 0, c, sumArray, 0, sumArray.Length);

			return sumArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Adds corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void AddAndReplace(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Add_IV64fV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Adds a constant to double precision (64-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void AddAndReplace(this double[] xArray, double c)
		{
			Yeppp.Core.Add_IV64fS64f_IV64f(xArray, 0, c, xArray.Length);
		}
		#endregion
	}
}