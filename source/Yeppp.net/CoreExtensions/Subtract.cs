﻿namespace System
{
	public static class CoreExtensions_Subtract
	{
		#region int
		/// <summary>Subtracts corresponding elements in two signed 32-bit integer arrays. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static int[] Subtract(this int[] xArray, int[] yArray)
		{
			int[] diffArray = new int[xArray.Length];

			Yeppp.Core.Subtract_V32sV32s_V32s(xArray, 0, yArray, 0, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to signed 32-bit integer array elements. Produces an array of signed 32-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static int[] Subtract(this int[] xArray, int c)
		{
			int[] diffArray = new int[xArray.Length];

			Yeppp.Core.Subtract_V32sS32s_V32s(xArray, 0, c, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two signed 32-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Subtract_IV32sV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two signed 32-bit integer arrays and writes the result to the second array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace2nd(this int[] xArray, int[] yArray)
		{
			Yeppp.Core.Subtract_V32sIV32s_IV32s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to signed 32-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void SubtractAndReplace(this int[] xArray, int c)
		{
			Yeppp.Core.Subtract_IV32sS32s_IV32s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region long
		/// <summary>Subtracts corresponding elements in two signed 64-bit integer arrays. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static long[] Subtract(this long[] xArray, long[] yArray)
		{
			long[] diffArray = new long[xArray.Length];

			Yeppp.Core.Subtract_V64sV64s_V64s(xArray, 0, yArray, 0, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to signed 64-bit integer array elements. Produces an array of signed 64-bit integer elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static long[] Subtract(this long[] xArray, long c)
		{
			long[] diffArray = new long[xArray.Length];

			Yeppp.Core.Subtract_V64sS64s_V64s(xArray, 0, c, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two signed 64-bit integer arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace(this long[] xArray, long[] yArray)
		{
			Yeppp.Core.Subtract_IV64sV64s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two signed 64-bit integer arrays and writes the result to the second array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace2nd(this long[] xArray, long[] yArray)
		{
			Yeppp.Core.Subtract_V64sIV64s_IV64s(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to signed 64-bit integer array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void SubtractAndReplace(this long[] xArray, long c)
		{
			Yeppp.Core.Subtract_IV64sS64s_IV64s(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region float
		/// <summary>Subtracts corresponding elements in two single precision (32-bit) floating-point arrays. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static float[] Subtract(this float[] xArray, float[] yArray)
		{
			float[] diffArray = new float[xArray.Length];

			Yeppp.Core.Subtract_V32fV32f_V32f(xArray, 0, yArray, 0, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to single precision (32-bit) floating-point array elements. Produces an array of single precision (32-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static float[] Subtract(this float[] xArray, float c)
		{
			float[] diffArray = new float[xArray.Length];

			Yeppp.Core.Subtract_V32fS32f_V32f(xArray, 0, c, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Subtract_IV32fV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two single precision (32-bit) floating-point arrays and writes the result to the second array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace2nd(this float[] xArray, float[] yArray)
		{
			Yeppp.Core.Subtract_V32fIV32f_IV32f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to single precision (32-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void SubtractAndReplace(this float[] xArray, float c)
		{
			Yeppp.Core.Subtract_IV32fS32f_IV32f(xArray, 0, c, xArray.Length);
		}
		#endregion
		//---------------------------------------------------------------------
		#region double
		/// <summary>Subtracts corresponding elements in two double precision (64-bit) floating-point arrays. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray, yArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray, yArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static double[] Subtract(this double[] xArray, double[] yArray)
		{
			double[] diffArray = new double[xArray.Length];

			Yeppp.Core.Subtract_V64fV64f_V64f(xArray, 0, yArray, 0, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to double precision (64-bit) floating-point array elements. Produces an array of double precision (64-bit) floating-point elements.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or diffArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or diffArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, sumOffset is negative, sumOffset + length exceeds the length of diffArray, or length is negative.</exception>
		public static double[] Subtract(this double[] xArray, double c)
		{
			double[] diffArray = new double[xArray.Length];

			Yeppp.Core.Subtract_V64fS64f_V64f(xArray, 0, c, diffArray, 0, diffArray.Length);

			return diffArray;
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the first array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Subtract_IV64fV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts corresponding elements in two double precision (64-bit) floating-point arrays and writes the result to the second array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray or yArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray or yArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, yOffset is negative, yOffset + length exceeds the length of yArray, or length is negative.</exception>
		public static void SubtractAndReplace2nd(this double[] xArray, double[] yArray)
		{
			Yeppp.Core.Subtract_V64fIV64f_IV64f(xArray, 0, yArray, 0, xArray.Length);
		}
		//---------------------------------------------------------------------
		/// <summary>Subtracts a constant to double precision (64-bit) floating-point array elements and writes the result to the same array.</summary>
		/// <exception cref="System.NullReferenceException">If xArray is null.</exception>
		/// <exception cref="System.DataMisalignedException">If xArray is not naturally aligned.</exception>
		/// <exception cref="System.ArgumentException">If length is negative.</exception>
		/// <exception cref="System.IndexOutOfRangeException">If xOffset is negative, xOffset + length exceeds the length of xArray, or length is negative.</exception>
		public static void SubtractAndReplace(this double[] xArray, double c)
		{
			Yeppp.Core.Subtract_IV64fS64f_IV64f(xArray, 0, c, xArray.Length);
		}
		#endregion
	}
}